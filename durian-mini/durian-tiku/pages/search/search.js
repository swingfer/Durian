// pages/search/search.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    searchflag: false,
    searchPlaceholder: "搜索",
    showQuestionList: true,
    searchViewHeight: "100%",
    scrollViewHeight: "0%",
    searchValue: "",
    questions: []
  },

  /**
   * 获得焦点
   */
  getfocus() {
    this.setData({
      searchflag: true,
      searchViewHeight: "10%",
      scrollViewHeight: "90%",
      searchPlaceholder: ""
    })
  },

  /**
   * 失去焦点
   */
  loseFocus() {
    //如果搜索框内的数据为空，则去掉清除和搜索图标
    if (this.data.searchValue == "") {
      this.setData({
        searchflag: false,
        searchPlaceholder: "搜索",
      })
    }
  },

  /**
   * 清除搜索框内容
   */
  cleanInput() {
    this.setData({
      searchValue: ""
    })
  },

  /**
   * 搜索
   */
  search() {
    let key = "ainimemeda";
    let pageStart = 0;
    let pageRows = 10;
    let condition = this.data.searchValue;
    const url = `https://qiu-qian.top:8088/solr/tiku/question/search?condition=${condition}&pageStart=${pageStart}&pageRows=${pageRows}&key=${key}`
    var this1 = this;
    wx.showLoading({
      title: '搜索中',
    })
    wx.request({
      url: url,
      method: 'GET',
      dataType: 'json',
      success: function (res) {
        wx.hideLoading()
        this1.setData({
          questions: res.data.body.questions,
        })
      },
      fail: function (res) {
        wx.hideLoading()
        console.log(res);
        console.log("请求失败");
      }
    })
  },

  /**
   * 点击获取题目详情
   */
  getDetail: function (e) {
    let questionId = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/detail/detail?questionId=' + questionId,
    })
  }

})