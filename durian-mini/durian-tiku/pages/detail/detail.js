// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //题目详细内容
    fullContent: "",
    //答案列表
    answers: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //从上一个页面获取出入的id
    let questionId = options.questionId
    let key = "ainimemeda";
    const url = `https://qiu-qian.top:8086/tiku/question?key=${key}&id=${questionId}`;
    wx.showLoading({
      title: '稍等',
    })
    var this1 = this
    wx.request({
      url: url,
      method: 'GET',
      dataType: 'json',
      success: function (res) {
        wx.hideLoading()
        console.log(res.data.body);
        this1.setData({
          fullContent: res.data.body.question.fullContent,
          answers: res.data.body.answerList,
        })
      },
      fail: function (res) {
        wx.hideLoading()
        console.log(res);
        console.log("请求失败");
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})