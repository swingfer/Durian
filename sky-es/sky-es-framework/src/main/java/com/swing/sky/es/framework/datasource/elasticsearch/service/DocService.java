package com.swing.sky.es.framework.datasource.elasticsearch.service;

import java.io.IOException;

/**
 * 操作ES文档信息的接口
 *
 * @author swing
 */
public interface DocService {
    /**
     * 更新文档信息
     *
     * @throws IOException
     */
    public void updateDoc() throws IOException;
}
