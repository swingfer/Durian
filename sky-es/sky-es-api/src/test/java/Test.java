import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;

import com.swing.sky.es.api.EsApplication;
import com.swing.sky.es.framework.datasource.elasticsearch.EsClient;
import com.swing.sky.es.framework.datasource.elasticsearch.service.DocService;
import com.swing.sky.es.module.service.TiQuestionSearchService;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EsApplication.class)
public class Test {
    // @Autowired
    // private CenterCourseService courseService;

    // @Autowired
    // private CenterDeptCourseLinkService deptCourseLinkService;

    // @Autowired
    // private TiQuestionService questionService;

    @Autowired
    private TiQuestionSearchService searchService;

    @Resource
    private EsClient esClient;

    @Resource
    private DocService tikuQuestionDocServiceImpl;

    @org.junit.Test
    public void test1() {
        // RestHighLevelClient restClient = esClient.getRestClient();
        searchService.listAll().forEach(System.out::println);
    }

    @org.junit.Test
    public void test2() throws IOException, SQLException {
        tikuQuestionDocServiceImpl.updateDoc();
    }
}
