package com.swing.sky.system.module.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.swing.sky.system.module.dao.SysUserOnlineDAO;
import com.swing.sky.system.module.domain.SysUserOnlineDO;
import com.swing.sky.system.module.service.SysUserOnlineService;

import org.springframework.stereotype.Service;

/**
 * @author swing
 */
@Service
public class SysUserOnlineServiceImpl implements SysUserOnlineService {

    @Resource
    private SysUserOnlineDAO userOnlineDAO;

    @Override
    public int insert(SysUserOnlineDO sysUserOnlineDO) {
        if (userOnlineDAO.countBySessionId(sysUserOnlineDO.getSessionId(), -1L) > 0) {
            throw new RuntimeException("该session已存在");
        }
        return userOnlineDAO.insert(sysUserOnlineDO);
    }

    @Override
    public int deleteById(Long id) {
        return userOnlineDAO.deleteById(id);
    }

    @Override
    public int batchDeleteByIds(Long[] ids) {
        return userOnlineDAO.batchDeleteByIds(ids);
    }

    @Override
    public int update(SysUserOnlineDO sysUserOnlineDO) {
        if (userOnlineDAO.countBySessionId(sysUserOnlineDO.getSessionId(), sysUserOnlineDO.getId()) > 0) {
            throw new RuntimeException("该session已存在");
        }
        return userOnlineDAO.update(sysUserOnlineDO);
    }

    @Override
    public SysUserOnlineDO getById(Long id) {
        return userOnlineDAO.getById(id);
    }

    @Override
    public List<SysUserOnlineDO> listByCondition(SysUserOnlineDO sysUserOnlineDO, String beginTime, String endTime) {
        return userOnlineDAO.listByCondition(sysUserOnlineDO, beginTime, endTime);
    }
}
