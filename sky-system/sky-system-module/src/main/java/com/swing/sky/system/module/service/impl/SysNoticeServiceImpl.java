package com.swing.sky.system.module.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.swing.sky.system.module.annotation.SkyServiceAuthority;
import com.swing.sky.system.module.dao.SysNoticeDAO;
import com.swing.sky.system.module.domain.SysNoticeDO;
import com.swing.sky.system.module.service.SysNoticeService;

import org.springframework.stereotype.Service;

/**
 * @author swing
 */
@Service
public class SysNoticeServiceImpl implements SysNoticeService {

    @Resource
    private SysNoticeDAO noticeDAO;

    @Override
    @SkyServiceAuthority(moduleName = "notice")
    public int insert(SysNoticeDO sysNoticeDO) {
        return noticeDAO.insert(sysNoticeDO);
    }

    @Override
    public int deleteById(Long id) {
        return noticeDAO.deleteById(id);
    }

    @Override
    public int batchDeleteByIds(Long[] ids) {
        return noticeDAO.batchDeleteByIds(ids);
    }

    @Override
    @SkyServiceAuthority(moduleName = "notice")
    public int update(SysNoticeDO sysNoticeDO) {
        return noticeDAO.update(sysNoticeDO);
    }

    @Override
    public SysNoticeDO getById(Long id) {
        return noticeDAO.getById(id);
    }

    @Override
    public List<SysNoticeDO> listByCondition(SysNoticeDO sysNoticeDO, String beginTime, String endTime) {
        return noticeDAO.listByCondition(sysNoticeDO, beginTime, endTime);
    }
}
