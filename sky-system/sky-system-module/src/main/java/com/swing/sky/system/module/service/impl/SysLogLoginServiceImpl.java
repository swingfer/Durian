package com.swing.sky.system.module.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.swing.sky.system.module.dao.SysLogLoginDAO;
import com.swing.sky.system.module.domain.SysLogLoginDO;
import com.swing.sky.system.module.service.SysLogLoginService;

import org.springframework.stereotype.Service;

/**
 * @author swing
 */
@Service
public class SysLogLoginServiceImpl implements SysLogLoginService {

    @Resource
    private SysLogLoginDAO logLoginDAO;

    @Override
    public int insert(SysLogLoginDO sysLogLoginDO) {
        return logLoginDAO.insert(sysLogLoginDO);
    }

    @Override
    public int deleteById(Long id) {
        return logLoginDAO.deleteById(id);
    }

    @Override
    public int batchDeleteByIds(Long[] ids) {
        return logLoginDAO.batchDeleteByIds(ids);
    }

    @Override
    public int update(SysLogLoginDO sysLogLoginDO) {
        return logLoginDAO.update(sysLogLoginDO);
    }

    @Override
    public SysLogLoginDO getById(Long id) {
        return logLoginDAO.getById(id);
    }

    @Override
    public List<SysLogLoginDO> listByCondition(SysLogLoginDO sysLogLoginDO, String beginTime, String endTime) {
        return logLoginDAO.listByCondition(sysLogLoginDO, beginTime, endTime);
    }
}
